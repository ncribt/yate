mod assets;
mod commands;
mod components;
mod delegate;
mod states;
mod theme;

use std::{env::var, path::Path};

use delegate::Delegate;
use druid::{
  commands as default_commands, theme as default_theme, AppLauncher, Command,
  Env, FileDialogOptions, Insets, Menu, MenuItem, PlatformError, Target,
  WindowDesc, WindowState,
};

use states::{ExplorerState, RootState};

/**
 * TODO: For MVP
 * - Edit and save file changes
 * - Listen to Ctrl+S shortcut to write changes to file
 * - ✅ Add syntax highlighting for Rust
 * - Add status bar (syntax, spaces/tabs, line ending, line/col, encoding, git branch)
 * - ✅ Add line numbers
 * - Add scroll map
 * - Add tabs
 * - Text selection (double click on word should select the word)
 * - Copy/pasting
 * - Detect indentation (tab should insert the correct indentation type) (deleting should delete the whole indentation)
 * - Auto-insert matching brackets, parenthesis, quotes
 * - Detect file changes and reload if necessary
 * - Show warning if quitting with unsaved changes
 * - Add settings (can be overwritten by workspace) (coming from JSON) to set things like font size, font family, indentation, theme, etc
 * - Allow using different system fonts
 * - Better icons
 * - ✅ Functioning scrollbar
 * - Horizontal scrollbar
 * - Keep selection in explorer
 * - Show editor tabs (with saved status)
 * - Moving the cursor past the visible area should automatically scroll (same with Enter and Backspace)
 * - Listen to other keys like (home, end, del, page up page down, etc)
 */

/**
 * TODO: Explorer improvements/features
 * - Create a new file/dir
 * - Copy, cut and or paste a file or dir
 * - Rename file/dir
 */

/**
 * TODO: Editor improvements/features
 * - Search (Ctrl+F)
 * - ✅ Blinking cursor
 * - ✅ Highlight selected line
 * - Multiple cursors
 * - Interface with rust-analyzer (and LSP?) to handle linting and formatting
 * - Check spelling (using local dictionary)
 * - Allow using VSCode themes
 */

fn main() -> Result<(), PlatformError> {
  let main_window = WindowDesc::new(components::app())
    .title("Yate")
    .set_window_state(WindowState::Maximized)
    .menu(|_, _, _| {
      Menu::empty().entry(Menu::new("File").entry(
        MenuItem::new("Open folder...").command(Command::new(
          default_commands::SHOW_OPEN_PANEL,
          FileDialogOptions::select_directories(FileDialogOptions::new()),
          Target::Auto,
        )),
      ))
    });

  let root_state = RootState {
    explorer_state: ExplorerState::new(
      // FIXME: Remove this hardcoded value
      Path::join(
        Path::new(&var("HOME").unwrap()),
        Path::new("./dev/ncrb/yate"),
      )
      .to_str()
      .unwrap()
      .to_string(),
    ),
    ..RootState::default()
  };

  AppLauncher::with_window(main_window)
    .configure_env(|env: &mut Env, _data: &RootState| {
      env.set(default_theme::TEXT_COLOR, theme::PALETTE.text.clone());
      env.set(default_theme::TEXT_SIZE_NORMAL, theme::FONT.font_sizes.normal);
      env.set(default_theme::BACKGROUND_DARK, theme::PALETTE.background_2);
      env.set(default_theme::BACKGROUND_LIGHT, theme::PALETTE.background);
      env.set(default_theme::TEXTBOX_BORDER_RADIUS, 0.0);
      env.set(default_theme::TEXTBOX_BORDER_WIDTH, 0.0);
      env
        .set(default_theme::TEXTBOX_INSETS, Insets::uniform(theme::SPACING.x1));
    })
    .delegate(Delegate {})
    .launch(root_state)?;

  Ok(())
}
