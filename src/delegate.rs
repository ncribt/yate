use druid::{
  commands as default_commands, AppDelegate, Command, DelegateCtx, Env,
  Handled, Target,
};

use crate::{
  commands,
  states::{ExplorerState, RootState},
};

pub struct Delegate {}

impl AppDelegate<RootState> for Delegate {
  fn command(
    &mut self,
    _ctx: &mut DelegateCtx,
    _target: Target,
    cmd: &Command,
    root_state: &mut RootState,
    _env: &Env,
  ) -> Handled {
    if let Some(file_info) = cmd.get(default_commands::OPEN_FILE) {
      root_state.explorer_state =
        ExplorerState::new(file_info.path.to_str().unwrap().to_string());

      return Handled::Yes;
    }

    if let Some(data) = cmd.get(commands::EXPLORER_CLICK_FILE) {
      root_state.explorer_state.select_path(data.path.clone());
      root_state.editor_state.focus_file(data.path.clone());

      return Handled::Yes;
    }

    if let Some(data) = cmd.get(commands::EXPLORER_CLICK_DIRECTORY) {
      root_state.explorer_state.select_path(data.path.clone());

      return Handled::Yes;
    }

    if let Some((path, content)) = cmd.get(commands::EDITOR_CACHE_FILE_CONTENT)
    {
      root_state.editor_state.cache_file_content(path.clone(), content.clone());
      return Handled::Yes;
    }

    Handled::No
  }
}
