use druid::{
  widget::{Padding, Scroll, Split},
  Insets, LensExt, Widget, WidgetExt,
};

use crate::{
  states::{ExplorerState, RootState},
  theme,
};

use super::{editor, explorer};

pub fn app() -> impl Widget<RootState> {
  Split::columns(
    Scroll::new(Padding::new(
      Insets::new(0.0, 0.0, 0.0, theme::SPACING.x8),
      explorer(0.0).lens(LensExt::then(
        RootState::explorer_state,
        ExplorerState::directory,
      )),
    ))
    .vertical()
    .background(theme::PALETTE.background_2),
    editor().lens(RootState::editor_state),
  )
  .split_point(0.1)
  .bar_size(0.0)
  .draggable(true)
  .solid_bar(true)
  .background(theme::PALETTE.background)
}
