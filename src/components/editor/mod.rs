mod caret;
#[path = "editor.rs"]
mod editor_component;
#[path = "editor-controller.rs"]
mod editor_controller;
mod gutter;
mod metrics;
#[path = "scroll-bar.rs"]
mod scroll_bar;
mod syntax;
mod utils;

use druid::{
  lens,
  widget::{Flex, Maybe},
  Widget, WidgetExt, WidgetId,
};

use crate::{states::EditorState, theme};

use self::{
  editor_component::Editor, editor_controller::EditorController,
  gutter::Gutter, scroll_bar::ScrollBar,
};

pub fn editor() -> impl Widget<EditorState> {
  let editor_id = WidgetId::next();
  let scroll_bar_id = WidgetId::next();
  let gutter_id = WidgetId::next();

  Maybe::new(
    move || {
      Flex::row()
        .with_child(
          Gutter::default().with_id(gutter_id).fix_width(theme::SPACING.x16),
        )
        .with_flex_child(Editor::default().with_id(editor_id), 1.0)
        .with_child(
          ScrollBar::default()
            .with_id(scroll_bar_id)
            .fix_width(theme::SPACING.x4),
        )
        .controller(EditorController { editor_id, scroll_bar_id, gutter_id })
        .expand()
    },
    || Flex::row().expand(),
  )
  .lens(lens::Map::new(
    |editor_state: &EditorState| editor_state.get_focused_file(),
    |editor_state: &mut EditorState, file| {
      if let Some(file) = file {
        editor_state.update_focused_file(file);
      }
    },
  ))
}
