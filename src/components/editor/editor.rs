use std::{fs::File, io::BufReader, path::PathBuf};

use druid::{
  keyboard_types,
  piet::{PietText, Text, TextAttribute, TextLayout, TextLayoutBuilder},
  BoxConstraints, Color, Event, FontFamily, FontWeight, LifeCycle, MouseButton,
  Rect, RenderContext, Size, Target, Widget,
};
use ropey::Rope;
use syntect::{easy::HighlightLines, highlighting, highlighting::FontStyle};

use crate::{commands, states::EditorFileState, theme};

use super::{
  caret::Caret,
  metrics::Metrics,
  syntax::Syntax,
  utils::{get_closest_line_idx, get_closest_relative_char_idx},
};

// TODO: Use absolute char index instead of relative (so line index is not necessary)
pub struct Editor {
  pub offset_y: f64,

  pub text: Rope,

  pub bg_color: Color,
  pub selected_line_color: Color,

  pub font_family: FontFamily,
  pub font_size: f64,

  pub caret: Caret,
  pub metrics: Metrics,
  pub syntax: Syntax,

  pub is_dragging_scroll_bar: bool,
}

impl Default for Editor {
  fn default() -> Self {
    Self {
      offset_y: 0.0,
      font_family: FontFamily::MONOSPACE,
      font_size: theme::FONT.font_sizes.normal,
      text: Rope::new(),
      bg_color: theme::PALETTE.background,
      selected_line_color: Color::rgba8(255, 255, 255, 25),
      caret: Caret::default(),
      metrics: Metrics::default(),
      syntax: Syntax::default(),
      is_dragging_scroll_bar: false,
    }
  }
}

impl Editor {
  pub fn init_metrics(&mut self, ctx: &mut PietText, bc: &BoxConstraints) {
    let layout = ctx
      .new_text_layout("0")
      .font(self.font_family.clone(), self.font_size)
      .build()
      .unwrap();

    let line_height = layout.line_metric(0).unwrap().height;
    let char_width = layout.size().width;
    let box_height = bc.max().height;
    let box_width = bc.max().width;

    let metrics = Metrics::new(line_height, char_width, box_height, box_width);

    self.metrics = metrics;
    self.caret.init(self.syntax.clone(), self.metrics.clone());
  }

  pub fn init_syntax(&mut self, path: String) {
    let extension = path.split('.').last().unwrap_or("txt");

    let syntax = Syntax::new(extension.to_string());

    self.syntax = syntax;
    self.caret.init(self.syntax.clone(), self.metrics.clone());

    let syntax_theme_bg_color = self.syntax.theme.settings.background.unwrap();

    let bg_color = Color::rgba8(
      syntax_theme_bg_color.r,
      syntax_theme_bg_color.g,
      syntax_theme_bg_color.b,
      syntax_theme_bg_color.a,
    );

    self.bg_color = bg_color;

    let syntax_theme_selected_line_color = self
      .syntax
      .theme
      .settings
      .active_guide
      .unwrap_or(highlighting::Color::WHITE);

    let selected_line_color = Color::rgba8(
      syntax_theme_selected_line_color.r,
      syntax_theme_selected_line_color.g,
      syntax_theme_selected_line_color.b,
      25,
    );

    self.selected_line_color = selected_line_color;
  }

  pub fn load_text(&mut self, path: PathBuf) {
    self.text =
      Rope::from_reader(BufReader::new(File::open(path).unwrap())).unwrap();
  }
}

impl Widget<EditorFileState> for Editor {
  fn event(
    &mut self,
    ctx: &mut druid::EventCtx,
    event: &druid::Event,
    _data: &mut EditorFileState,
    _env: &druid::Env,
  ) {
    let handled = self.caret.handle_event(ctx, event);

    if handled {
      ctx.set_handled();
      return;
    }

    match event {
      Event::KeyDown(event) => {
        /*
        println!(
          "{} {} {:?} {}",
          event.key, event.code, event.mods, event.repeat
        );
        */

        if let Some((line_idx, relative_char_idx)) = self.caret.position {
          // TODO: Implement auto insertion of matching chars (curly brackets, parenthesis, quotes, double quotes, square brackets)

          if event.code == keyboard_types::Code::ArrowLeft {
            if relative_char_idx == 0 && line_idx == 0 {
              // Do nothing
              ctx.is_handled();
              return;
            }

            if relative_char_idx == 0 && line_idx > 0 {
              let prev_line = self.text.line(line_idx - 1);
              let prev_line_chars = prev_line.chars();
              let prev_line_chars_count = prev_line_chars.clone().count();
              let mut prev_line_last_char_idx = prev_line_chars_count;

              if prev_line_chars_count > 0
                && prev_line_chars.last().unwrap() == '\n'
              {
                prev_line_last_char_idx = prev_line_chars_count - 1;
              }

              self.caret.set_position(line_idx - 1, prev_line_last_char_idx);
              self.caret.show_and_reset_timer(ctx);

              ctx.request_paint();
              ctx.is_handled();
              return;
            }

            self.caret.set_position(line_idx, relative_char_idx - 1);
            self.caret.show_and_reset_timer(ctx);

            ctx.request_paint();
            ctx.is_handled();
            return;
          }

          if event.code == keyboard_types::Code::ArrowRight {
            let lines_count = self.text.len_lines();
            let curr_line = self.text.line(line_idx);
            let curr_line_chars = curr_line.chars();
            let curr_line_chars_count = curr_line_chars.clone().count();
            let mut curr_line_last_char_idx = curr_line_chars_count;

            if curr_line_chars_count > 0
              && curr_line_chars.last().unwrap() == '\n'
            {
              curr_line_last_char_idx = curr_line_chars_count - 1;
            }

            if relative_char_idx == curr_line_last_char_idx
              && line_idx == lines_count - 1
            {
              // Do nothing
              ctx.is_handled();
              return;
            }

            if relative_char_idx == curr_line_last_char_idx
              && line_idx < lines_count - 1
            {
              self.caret.set_position(line_idx + 1, 0);
              self.caret.show_and_reset_timer(ctx);

              ctx.request_paint();
              ctx.is_handled();
              return;
            }

            self.caret.set_position(line_idx, relative_char_idx + 1);
            self.caret.show_and_reset_timer(ctx);

            ctx.request_paint();
            ctx.is_handled();
            return;
          }

          if event.code == keyboard_types::Code::ArrowUp {
            if line_idx == 0 {
              // Do nothing
              ctx.is_handled();
              return;
            }

            let prev_line = self.text.line(line_idx - 1);
            let prev_line_chars = prev_line.chars();
            let prev_line_chars_count = prev_line_chars.clone().count();
            let mut prev_line_last_char_idx = prev_line_chars_count;

            if prev_line_chars_count > 0
              && prev_line_chars.last().unwrap() == '\n'
            {
              prev_line_last_char_idx = prev_line_chars_count - 1;
            }

            if prev_line_last_char_idx >= relative_char_idx {
              self.caret.set_position(line_idx - 1, relative_char_idx);
            } else {
              self.caret.set_position(line_idx - 1, prev_line_last_char_idx);
            }

            self.caret.show_and_reset_timer(ctx);

            ctx.request_paint();
            ctx.is_handled();
            return;
          }

          if event.code == keyboard_types::Code::ArrowDown {
            let lines_count = self.text.len_lines();

            if line_idx == lines_count - 1 {
              // Do nothing
              ctx.is_handled();
              return;
            }

            let next_line = self.text.line(line_idx + 1);
            let next_line_chars = next_line.chars();
            let next_line_chars_count = next_line_chars.clone().count();
            let mut next_line_last_char_idx = next_line_chars_count;

            if next_line_chars_count > 0
              && next_line_chars.last().unwrap() == '\n'
            {
              next_line_last_char_idx = next_line_chars_count - 1;
            }

            if next_line_last_char_idx >= relative_char_idx {
              self.caret.set_position(line_idx + 1, relative_char_idx);
            } else {
              self.caret.set_position(line_idx + 1, next_line_last_char_idx);
            }

            self.caret.show_and_reset_timer(ctx);

            ctx.request_paint();
            ctx.is_handled();
            return;
          }

          // TODO: Implement "smart" new line (keep indentation of current block)
          if event.code == keyboard_types::Code::Enter {
            let line_first_char_idx = self.text.line_to_char(line_idx);
            let char_idx = line_first_char_idx + relative_char_idx;

            self.text.insert_char(char_idx, '\n');
            self.caret.set_position(line_idx + 1, 0);
            self.caret.show_and_reset_timer(ctx);

            ctx.submit_command(
              commands::EDITOR_LEN_LINES_CHANGED.with(self.text.len_lines()),
            );

            ctx.request_paint();
            ctx.is_handled();
            return;
          }

          // TODO: Implement "smart" delete (remove indent)
          if event.code == keyboard_types::Code::Backspace {
            let line_first_char_idx = self.text.line_to_char(line_idx);
            let char_idx = line_first_char_idx + relative_char_idx;

            if char_idx > 0 {
              if relative_char_idx == 0 {
                let prev_line = self.text.line(line_idx - 1);
                let prev_line_chars = prev_line.chars();
                let prev_line_chars_count = prev_line_chars.clone().count();
                let mut prev_line_last_char_idx = prev_line_chars_count;

                if prev_line_chars_count > 0
                  && prev_line_chars.last().unwrap() == '\n'
                {
                  prev_line_last_char_idx = prev_line_chars_count - 1;
                }

                self.caret.set_position(line_idx - 1, prev_line_last_char_idx);
              } else {
                self.caret.set_position(line_idx, relative_char_idx - 1);
              }

              self.text.remove((char_idx - 1)..char_idx);

              if relative_char_idx == 0 {
                ctx.submit_command(
                  commands::EDITOR_LEN_LINES_CHANGED
                    .with(self.text.len_lines()),
                );
              }

              ctx.request_paint();
            }

            ctx.is_handled();
            return;
          }

          if let keyboard_types::Key::Character(text) = event.key.clone() {
            if event.mods.ctrl() || event.mods.alt() || event.mods.meta() {
              return;
            }

            let line_first_char_idx = self.text.line_to_char(line_idx);
            let char_idx = line_first_char_idx + relative_char_idx;

            self.text.insert(char_idx, &text);
            self.caret.set_position(line_idx, relative_char_idx + 1);

            ctx.request_paint();
            ctx.is_handled();
          }
        }
      }
      Event::MouseDown(event) => {
        if event.button == MouseButton::Left {
          let line_idx = get_closest_line_idx(
            event.pos.y,
            self.offset_y,
            self.metrics.line_height,
            self.text.len_lines(),
          );

          let relative_char_idx = get_closest_relative_char_idx(
            event.pos.x,
            self.metrics.char_width,
            self.text.line(line_idx),
          );

          self.caret.set_position(line_idx, relative_char_idx);

          ctx.request_paint();
        }

        ctx.request_focus();
      }
      // FIXME: Scrolling does not feel smooth -> Seems to be because of the paint function, small files with no syntax work fine
      // Seems to be OK using "release" build
      Event::Command(cmd) if cmd.is(commands::EDITOR_SCROLL) => {
        let (_x, y) = *cmd.get_unchecked(commands::EDITOR_SCROLL);

        self.offset_y = (self.offset_y + y / 2.0)
          .clamp(0.0, self.text.len_lines() as f64 * self.metrics.line_height);

        ctx.request_paint();
        ctx.set_handled();
      }
      Event::Command(cmd) if cmd.is(commands::EDITOR_SCROLL_TO) => {
        let (_x, y) = cmd.get_unchecked(commands::EDITOR_SCROLL_TO);

        self.offset_y = *y;

        ctx.request_paint();
        ctx.set_handled();
      }
      Event::Command(cmd) if cmd.is(commands::EDITOR_SET_CURSOR) => {
        ctx.clear_cursor();

        if !self.is_dragging_scroll_bar {
          ctx.set_cursor(&druid::Cursor::IBeam);
        }

        ctx.set_handled();
      }
      Event::Command(cmd)
        if cmd.is(commands::EDITOR_SET_DRAGGING_SCROLL_BAR) =>
      {
        self.is_dragging_scroll_bar =
          *cmd.get_unchecked(commands::EDITOR_SET_DRAGGING_SCROLL_BAR);

        ctx.submit_command(commands::EDITOR_SET_CURSOR);

        ctx.set_handled();
      }
      _ => (),
    }
  }

  fn lifecycle(
    &mut self,
    ctx: &mut druid::LifeCycleCtx,
    event: &druid::LifeCycle,
    data: &EditorFileState,
    _env: &druid::Env,
  ) {
    match event {
      LifeCycle::WidgetAdded => {
        self.load_text(PathBuf::from(&data.path));
        self.init_syntax(data.path.clone());

        ctx.submit_command(commands::EDITOR_RESET_GUTTER);
        ctx.submit_command(commands::EDITOR_RESET_SCROLL_BAR);

        ctx.submit_command(
          commands::EDITOR_LEN_LINES_CHANGED.with(self.text.len_lines()),
        );

        ctx.request_paint();
      }
      LifeCycle::BuildFocusChain => {
        ctx.register_for_focus();
      }
      LifeCycle::FocusChanged(focus) => {
        if *focus {
          ctx.submit_command(commands::EDITOR_SET_CURSOR.to(ctx.widget_id()));
        } else {
          self.caret.init(self.syntax.clone(), self.metrics.clone());
          ctx.request_paint();
        }
      }
      LifeCycle::HotChanged(hot) => {
        if *hot {
          ctx.submit_command(commands::EDITOR_SET_CURSOR.to(ctx.widget_id()));
        }
      }
      _ => (),
    }
  }

  fn update(
    &mut self,
    ctx: &mut druid::UpdateCtx,
    old_data: &EditorFileState,
    data: &EditorFileState,
    _env: &druid::Env,
  ) {
    if old_data.path != data.path {
      ctx.submit_command(
        commands::EDITOR_CACHE_FILE_CONTENT
          .with((old_data.path.clone(), self.text.clone()))
          .to(Target::Global),
      );

      if let Some(cached_content) = &data.cached_content {
        self.text = cached_content.clone();
      } else {
        self.load_text(PathBuf::from(&data.path));
      }

      ctx.submit_command(commands::EDITOR_RESET_GUTTER);
      ctx.submit_command(commands::EDITOR_RESET_SCROLL_BAR);

      ctx.submit_command(
        commands::EDITOR_LEN_LINES_CHANGED.with(self.text.len_lines()),
      );

      self.init_syntax(data.path.clone());

      self.offset_y = 0.0;
      self.caret.init(self.syntax.clone(), self.metrics.clone());

      ctx.request_paint();
    }
  }

  fn layout(
    &mut self,
    ctx: &mut druid::LayoutCtx,
    bc: &druid::BoxConstraints,
    _data: &EditorFileState,
    _env: &druid::Env,
  ) -> druid::Size {
    self.init_metrics(ctx.text(), bc);

    Size::new(self.metrics.box_width, self.metrics.box_height)
  }

  fn paint(
    &mut self,
    ctx: &mut druid::PaintCtx,
    _data: &EditorFileState,
    _env: &druid::Env,
  ) {
    let rect = ctx.size().to_rect();

    // Paint background
    ctx.render_ctx.fill(rect, &self.bg_color);

    // Paint selected line (if caret exists)
    if let Some((line_idx, _)) = self.caret.position {
      let y = line_idx as f64 * self.metrics.line_height - self.offset_y;

      ctx.render_ctx.fill(
        Rect::new(rect.x0, y, rect.x1, y + self.metrics.line_height),
        &self.selected_line_color,
      );
    }

    let lines = self.text.lines();

    let mut highlight_lines =
      HighlightLines::new(&self.syntax.reference, &self.syntax.theme);

    for (line_idx, line) in lines.enumerate() {
      let mut line_string = line.to_string();

      if let Some(line_without_suffix) = line_string.strip_suffix('\n') {
        line_string = line_without_suffix.to_string();
      }

      let mut layout = ctx
        .render_ctx
        .text()
        .new_text_layout(line_string.clone())
        .text_color(theme::PALETTE.text)
        .font(FontFamily::MONOSPACE, theme::FONT.font_sizes.normal);

      let ranges =
        highlight_lines.highlight_line(&line_string, &self.syntax.set).unwrap();

      let mut end_of_previous_range: usize = 0;

      for (style, text) in ranges {
        let color = TextAttribute::TextColor(Color::rgba8(
          style.foreground.r,
          style.foreground.g,
          style.foreground.b,
          style.foreground.a,
        ));

        layout = layout.range_attribute(
          end_of_previous_range..(end_of_previous_range + text.len()),
          color,
        );

        if style.font_style.contains(FontStyle::BOLD) {
          layout = layout.range_attribute(
            end_of_previous_range..(end_of_previous_range + text.len()),
            TextAttribute::Weight(FontWeight::BOLD),
          )
        }

        if style.font_style.contains(FontStyle::ITALIC) {
          layout = layout.range_attribute(
            end_of_previous_range..(end_of_previous_range + text.len()),
            TextAttribute::Style(druid::FontStyle::Italic),
          );
        }

        if style.font_style.contains(FontStyle::UNDERLINE) {
          layout = layout.range_attribute(
            end_of_previous_range..(end_of_previous_range + text.len()),
            TextAttribute::Underline(true),
          );
        }

        end_of_previous_range += text.len();
      }

      let layout = layout.build().unwrap();

      ctx.render_ctx.draw_text(
        &layout,
        (0.0, self.metrics.line_height * line_idx as f64 - self.offset_y),
      );
    }

    // Paint caret
    self.caret.paint(ctx, self.offset_y);
  }
}
