use ropey::RopeSlice;

pub fn get_closest_line_idx(
  y: f64,
  offset_y: f64,
  line_height: f64,
  lines_count: usize,
) -> usize {
  let offset_lines_count = (offset_y / line_height).floor();
  let mut line_idx = ((y / line_height).floor() + offset_lines_count) as usize;

  if line_idx >= lines_count {
    line_idx = lines_count - 1;
  }

  line_idx
}

pub fn get_closest_relative_char_idx(
  x: f64,
  char_width: f64,
  line: RopeSlice,
) -> usize {
  let mut chars_count = line.len_chars();
  let char_at_x = (x / char_width).round() as usize;

  if chars_count > 0 {
    let last_char = line.char(chars_count - 1);

    if last_char == '\n' && chars_count > 0 {
      chars_count -= 1;
    }
  }

  if chars_count < char_at_x {
    return chars_count;
  }

  char_at_x
}
