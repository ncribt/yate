use druid::{
  widget::{Controller, Flex},
  Env, Event, EventCtx, Target, Widget, WidgetId,
};

use crate::{commands, states::EditorFileState};

pub struct EditorController {
  pub editor_id: WidgetId,
  pub scroll_bar_id: WidgetId,
  pub gutter_id: WidgetId,
}

impl EditorController {
  fn send_scroll_to_command(&mut self, ctx: &mut EventCtx, x: f64, y: f64) {
    let widgets = vec![self.editor_id, self.scroll_bar_id, self.gutter_id];

    for widget in widgets {
      ctx.submit_command(
        commands::EDITOR_SCROLL_TO.with((x, y)).to(Target::Widget(widget)),
      );
    }
  }

  fn send_scroll_command(&mut self, ctx: &mut EventCtx, x: f64, y: f64) {
    let widgets = vec![self.editor_id, self.scroll_bar_id, self.gutter_id];

    for widget in widgets {
      ctx.submit_command(
        commands::EDITOR_SCROLL.with((x, y)).to(Target::Widget(widget)),
      );
    }
  }
}

impl Controller<EditorFileState, Flex<EditorFileState>> for EditorController {
  fn event(
    &mut self,
    child: &mut Flex<EditorFileState>,
    ctx: &mut EventCtx,
    event: &Event,
    data: &mut EditorFileState,
    env: &Env,
  ) {
    match event {
      Event::Command(cmd)
        if cmd.is(commands::EDITOR_SET_DRAGGING_SCROLL_BAR) =>
      {
        ctx.submit_command(
          commands::EDITOR_SET_DRAGGING_SCROLL_BAR
            .with(*cmd.get_unchecked(commands::EDITOR_SET_DRAGGING_SCROLL_BAR))
            .to(Target::Widget(self.editor_id)),
        );
      }
      Event::Command(cmd) if cmd.is(commands::EDITOR_SCROLL_TO) => {
        let (x, y) = cmd.get_unchecked(commands::EDITOR_SCROLL_TO);
        self.send_scroll_to_command(ctx, *x, *y);
      }
      Event::Wheel(m) => {
        self.send_scroll_command(ctx, m.wheel_delta.x, m.wheel_delta.y);
        ctx.set_handled();
      }
      _ => child.event(ctx, event, data, env),
    }
  }
}
