#[derive(Debug, Clone)]
pub struct Metrics {
  pub line_height: f64,
  pub char_width: f64,
  pub box_height: f64,
  pub box_width: f64,
}

impl Default for Metrics {
  fn default() -> Self {
    Self {
      line_height: 20.0,
      char_width: 8.0,
      box_height: 300.0,
      box_width: 400.0,
    }
  }
}

impl Metrics {
  pub fn new(
    line_height: f64,
    char_width: f64,
    box_height: f64,
    box_width: f64,
  ) -> Self {
    Self { line_height, char_width, box_height, box_width }
  }
}
