use druid::{
  piet::{Text, TextLayout, TextLayoutBuilder},
  Event, FontFamily, RenderContext, Size, Widget,
};

use crate::{commands, states::EditorFileState, theme};

pub struct Gutter {
  pub len_lines: usize,
  pub line_height: f64,
  pub vscroll: f64,
}

impl Default for Gutter {
  fn default() -> Self {
    Self { len_lines: 0, line_height: 20.0, vscroll: 0.0 }
  }
}

impl Widget<EditorFileState> for Gutter {
  fn event(
    &mut self,
    ctx: &mut druid::EventCtx,
    event: &druid::Event,
    _data: &mut EditorFileState,
    _env: &druid::Env,
  ) {
    match event {
      Event::Command(cmd) if cmd.is(commands::EDITOR_SCROLL) => {
        let (_x, y) = cmd.get_unchecked(commands::EDITOR_SCROLL);

        self.vscroll = (self.vscroll + y / 2.0)
          .clamp(0.0, self.len_lines as f64 * self.line_height);

        ctx.request_paint();
        ctx.set_handled();
      }
      Event::Command(cmd) if cmd.is(commands::EDITOR_SCROLL_TO) => {
        let (_x, y) = cmd.get_unchecked(commands::EDITOR_SCROLL_TO);

        self.vscroll = *y;

        ctx.request_paint();
        ctx.set_handled();
      }
      Event::Command(cmd) if cmd.is(commands::EDITOR_LEN_LINES_CHANGED) => {
        self.len_lines = *cmd.get_unchecked(commands::EDITOR_LEN_LINES_CHANGED);

        self.vscroll =
          self.vscroll.clamp(0.0, self.len_lines as f64 * self.line_height);

        ctx.request_paint();
      }
      Event::Command(cmd) if cmd.is(commands::EDITOR_RESET_GUTTER) => {
        self.vscroll = 0.0;
        self.len_lines = 0;

        ctx.request_paint();
        ctx.is_handled();
      }
      _ => (),
    }
  }

  fn lifecycle(
    &mut self,
    _ctx: &mut druid::LifeCycleCtx,
    _event: &druid::LifeCycle,
    _data: &EditorFileState,
    _env: &druid::Env,
  ) {
  }

  fn update(
    &mut self,
    _ctx: &mut druid::UpdateCtx,
    _old_data: &EditorFileState,
    _data: &EditorFileState,
    _env: &druid::Env,
  ) {
  }

  fn layout(
    &mut self,
    ctx: &mut druid::LayoutCtx,
    bc: &druid::BoxConstraints,
    _data: &EditorFileState,
    _env: &druid::Env,
  ) -> Size {
    let layout = ctx
      .text()
      .new_text_layout("0")
      .font(FontFamily::MONOSPACE, theme::FONT.font_sizes.normal)
      .build()
      .unwrap();

    self.line_height = layout.line_metric(0).unwrap().height;

    Size::new(bc.max().width, bc.max().height)
  }

  fn paint(
    &mut self,
    ctx: &mut druid::PaintCtx,
    _data: &EditorFileState,
    _env: &druid::Env,
  ) {
    let rect = ctx.size().to_rect();
    ctx.clip(rect);
    ctx.render_ctx.fill(rect, &theme::PALETTE.background);

    for line_idx in 0..(self.len_lines) {
      let layout = ctx
        .render_ctx
        .text()
        .new_text_layout((line_idx + 1).to_string())
        .text_color(theme::PALETTE.text)
        .font(FontFamily::MONOSPACE, theme::FONT.font_sizes.normal);

      let layout = layout.build().unwrap();

      ctx.render_ctx.draw_text(
        &layout,
        (
          rect.width() - layout.size().width - theme::SPACING.x4,
          self.line_height * line_idx as f64 - self.vscroll,
        ),
      );
    }
  }
}
