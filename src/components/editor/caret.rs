use std::time::Duration;

use druid::{Color, Event, MouseButton, Rect, RenderContext, Size, TimerToken};

use super::{metrics::Metrics, syntax::Syntax};

pub struct Caret {
  pub position: Option<(usize, usize)>,
  pub timer: Option<TimerToken>,
  pub show: bool,
  pub color: Color,
  pub size: Size,
  pub metrics: Metrics,
  pub syntax: Syntax,
}

impl Default for Caret {
  fn default() -> Self {
    Self {
      position: None,
      timer: None,
      show: false,
      color: Color::rgba8(255, 255, 255, 255),
      size: Size::new(2.0, 20.0),
      metrics: Metrics::default(),
      syntax: Syntax::default(),
    }
  }
}

impl Caret {
  pub fn init(&mut self, syntax: Syntax, metrics: Metrics) {
    let syntax_theme_caret_color = syntax.theme.settings.caret.unwrap();

    self.position = None;
    self.timer = None;
    self.show = false;
    self.color = Color::rgba8(
      syntax_theme_caret_color.r,
      syntax_theme_caret_color.g,
      syntax_theme_caret_color.b,
      syntax_theme_caret_color.a,
    );
    self.size = Size::new(2.0, metrics.line_height);
    self.metrics = metrics;
    self.syntax = syntax;
  }

  pub fn set_position(&mut self, line_idx: usize, relative_char_idx: usize) {
    self.position = Some((line_idx, relative_char_idx));
  }

  pub fn show_and_reset_timer(&mut self, ctx: &mut druid::EventCtx) {
    self.show = true;
    self.timer = Some(ctx.request_timer(Duration::from_millis(500)));
  }

  pub fn handle_event(
    &mut self,
    ctx: &mut druid::EventCtx,
    event: &druid::Event,
  ) -> bool {
    match event {
      Event::Timer(token) => {
        if let Some(timer_token) = self.timer {
          if *token == timer_token {
            self.show = !self.show;
            self.timer = Some(ctx.request_timer(Duration::from_millis(500)));

            ctx.request_paint();
            return true;
          }
        }

        false
      }
      Event::MouseDown(event) => {
        if event.button == MouseButton::Left {
          self.show = true;
          self.timer = Some(ctx.request_timer(Duration::from_millis(500)));

          ctx.request_paint();
        }

        false
      }
      _ => false,
    }
  }

  pub fn paint(&mut self, ctx: &mut druid::PaintCtx, offset_y: f64) {
    if let Some((line_idx, relative_char_idx)) = self.position {
      let y = line_idx as f64 * self.metrics.line_height - offset_y;
      let x = relative_char_idx as f64 * self.metrics.char_width;

      if self.show {
        ctx.render_ctx.fill(
          Rect::new(
            x - self.size.width / 2.0,
            y,
            x + self.size.width / 2.0,
            y + self.size.height,
          ),
          &self.color,
        );
      }
    }
  }
}
