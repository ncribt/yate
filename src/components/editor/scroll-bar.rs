use druid::{
  piet::{Text, TextLayout, TextLayoutBuilder},
  Color, Event, FontFamily, LifeCycle, Rect, RenderContext, Size, Widget,
};

use crate::{commands, states::EditorFileState, theme};

pub struct ScrollBar {
  pub len_lines: usize,
  pub line_height: f64,
  pub box_height: f64,
  pub vscroll: f64,
  pub is_dragging: bool,
  pub offset: f64,
}

impl Default for ScrollBar {
  fn default() -> Self {
    Self {
      len_lines: 0,
      line_height: 20.0,
      vscroll: 0.0,
      box_height: 400.0,
      is_dragging: false,
      offset: 0.0,
    }
  }
}

impl Widget<EditorFileState> for ScrollBar {
  fn event(
    &mut self,
    ctx: &mut druid::EventCtx,
    event: &druid::Event,
    _data: &mut EditorFileState,
    _env: &druid::Env,
  ) {
    match event {
      Event::MouseDown(m) => {
        self.is_dragging = true;

        let text_height = self.len_lines as f64 * self.line_height;
        let scroll_height = text_height + self.box_height;
        let ratio = self.box_height / scroll_height;
        let scroll_bar_height = self.box_height - text_height * ratio;
        let scroll_bar_y = self.vscroll * ratio;

        self.offset = scroll_bar_height / 2.0;

        if m.pos.y >= scroll_bar_y
          && m.pos.y <= scroll_bar_y + scroll_bar_height
        {
          self.offset = m.pos.y - scroll_bar_y;
        }

        let y = (m.pos.y - self.offset) / ratio;

        ctx.submit_command(
          commands::EDITOR_SCROLL_TO.with((
            0.0,
            y.clamp(0.0, self.len_lines as f64 * self.line_height),
          )),
        );

        ctx.submit_command(commands::EDITOR_SET_DRAGGING_SCROLL_BAR.with(true));

        ctx.request_focus();
        ctx.set_active(true);
        ctx.is_handled();
      }
      Event::MouseUp(_m) => {
        // FIXME: Bug when releasing button and outside the window or on title bar (MouseUp not triggered)
        self.is_dragging = false;
        self.offset = 0.0;

        ctx
          .submit_command(commands::EDITOR_SET_DRAGGING_SCROLL_BAR.with(false));

        ctx.set_active(false);
        ctx.is_handled();
      }
      Event::MouseMove(m) => {
        if self.is_dragging {
          let text_height = self.len_lines as f64 * self.line_height;
          let scroll_height = text_height + self.box_height;
          let ratio = self.box_height / scroll_height;
          let y = (m.pos.y - self.offset) / ratio;

          ctx.submit_command(commands::EDITOR_SCROLL_TO.with((
            0.0,
            y.clamp(0.0, self.len_lines as f64 * self.line_height),
          )));
        }

        ctx.request_paint();
        ctx.is_handled();
      }
      Event::Command(cmd) if cmd.is(commands::EDITOR_SCROLL) => {
        let (_x, y) = cmd.get_unchecked(commands::EDITOR_SCROLL);

        self.vscroll = (self.vscroll + y / 2.0)
          .clamp(0.0, self.len_lines as f64 * self.line_height);

        ctx.request_paint();
        ctx.set_handled();
      }
      Event::Command(cmd) if cmd.is(commands::EDITOR_SCROLL_TO) => {
        let (_x, y) = cmd.get_unchecked(commands::EDITOR_SCROLL_TO);

        self.vscroll = *y;

        ctx.request_paint();
        ctx.set_handled();
      }
      Event::Command(cmd) if cmd.is(commands::EDITOR_LEN_LINES_CHANGED) => {
        self.len_lines = *cmd.get_unchecked(commands::EDITOR_LEN_LINES_CHANGED);

        self.vscroll =
          self.vscroll.clamp(0.0, self.len_lines as f64 * self.line_height);

        ctx.request_paint();
      }
      Event::Command(cmd) if cmd.is(commands::EDITOR_RESET_SCROLL_BAR) => {
        self.vscroll = 0.0;
        self.len_lines = 0;

        ctx.request_paint();
        ctx.is_handled();
      }
      _ => (),
    }
  }

  fn lifecycle(
    &mut self,
    ctx: &mut druid::LifeCycleCtx,
    event: &druid::LifeCycle,
    _data: &EditorFileState,
    _env: &druid::Env,
  ) {
    if let LifeCycle::BuildFocusChain = event {
      ctx.register_for_focus();
    }
  }

  fn update(
    &mut self,
    _ctx: &mut druid::UpdateCtx,
    _old_data: &EditorFileState,
    _data: &EditorFileState,
    _env: &druid::Env,
  ) {
  }

  fn layout(
    &mut self,
    ctx: &mut druid::LayoutCtx,
    bc: &druid::BoxConstraints,
    _data: &EditorFileState,
    _env: &druid::Env,
  ) -> Size {
    let layout = ctx
      .text()
      .new_text_layout("0")
      .font(FontFamily::MONOSPACE, theme::FONT.font_sizes.normal)
      .build()
      .unwrap();

    self.line_height = layout.line_metric(0).unwrap().height;

    self.box_height = bc.max().height;

    Size::new(bc.max().width, self.box_height)
  }

  fn paint(
    &mut self,
    ctx: &mut druid::PaintCtx,
    _data: &EditorFileState,
    _env: &druid::Env,
  ) {
    let rect = ctx.size().to_rect();

    ctx.render_ctx.fill(rect, &theme::PALETTE.background_2);

    let text_height = self.len_lines as f64 * self.line_height;
    let scroll_height = text_height + self.box_height;
    let ratio = self.box_height / scroll_height;
    let scroll_bar_height = self.box_height - text_height * ratio;
    let y = self.vscroll * ratio;

    ctx.render_ctx.fill(
      Rect::new(rect.x0, y, rect.x1, y + scroll_bar_height),
      &Color::rgba8(255, 255, 255, 51),
    );
  }
}
