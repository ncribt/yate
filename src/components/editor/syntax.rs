use std::io::Cursor;

use syntect::{
  highlighting::{Theme, ThemeSet},
  parsing::{SyntaxReference, SyntaxSet},
};

use crate::assets;

#[derive(Debug, Clone)]
pub struct Syntax {
  pub set: SyntaxSet,
  pub reference: SyntaxReference,
  pub theme: Theme,
}

impl Default for Syntax {
  fn default() -> Self {
    let set = SyntaxSet::load_defaults_newlines();

    Self {
      set: set.clone(),
      reference: set.find_syntax_by_extension("txt").unwrap().clone(),
      theme: ThemeSet::load_from_reader(&mut Cursor::new(
        assets::GRUVBOX_THEME,
      ))
      .unwrap(),
    }
  }
}

impl Syntax {
  pub fn new(extension: String) -> Self {
    let set = SyntaxSet::load_defaults_newlines();

    let reference = match set.find_syntax_by_extension(&extension) {
      Some(s) => s.clone(),
      None => set.find_syntax_plain_text().clone(),
    };

    Self {
      set,
      reference,
      theme: ThemeSet::load_from_reader(&mut Cursor::new(
        assets::GRUVBOX_THEME,
      ))
      .unwrap(),
    }
  }
}
