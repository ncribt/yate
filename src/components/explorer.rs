use druid::{
  theme as default_theme,
  widget::{
    Container, CrossAxisAlignment, Flex, Label, List, MainAxisAlignment,
    Painter, Svg, SvgData, ViewSwitcher,
  },
  Color, Command, Insets, RenderContext, Target, Widget, WidgetExt,
};

use crate::{
  assets, commands,
  states::{ExplorerDirectoryState, ExplorerFileState},
  theme,
};

fn directory_button(insets: Insets) -> impl Widget<ExplorerDirectoryState> {
  Container::new(
    Flex::row()
      .main_axis_alignment(MainAxisAlignment::Start)
      .cross_axis_alignment(CrossAxisAlignment::Center)
      .with_child(
        ViewSwitcher::new(
          |data: &ExplorerDirectoryState, _| data.expanded,
          |selector, _, _| match selector {
            true => Box::new(Svg::new(
              assets::CHEVRON_DOWN_ICON.parse::<SvgData>().unwrap(),
            )),
            false => Box::new(Svg::new(
              assets::CHEVRON_RIGHT_ICON.parse::<SvgData>().unwrap(),
            )),
          },
        )
        .fix_height(default_theme::TEXT_SIZE_NORMAL),
      )
      .with_spacer(theme::SPACING.x1)
      .with_child(
        Label::new(|data: &ExplorerDirectoryState, _env: &_| data.name.clone())
          .with_text_size(default_theme::TEXT_SIZE_NORMAL)
          .expand_width(),
      ),
  )
  .expand_width()
  .padding(insets)
  .background(Painter::new(|ctx, _, _| {
    if ctx.is_active() {
      ctx
        .render_ctx
        .fill(ctx.size().to_rect(), &Color::rgba8(255, 255, 255, 13));
    }
  }))
  .on_click(|ctx, data, _| {
    ctx.set_active(true);

    data.toggle_expanded();

    ctx.submit_command(Command::new(
      commands::EXPLORER_CLICK_DIRECTORY,
      data.clone(),
      Target::Auto,
    ));
  })
}

fn directory_entry(
  indent: f64,
  insets: Insets,
) -> impl Widget<ExplorerDirectoryState> {
  ViewSwitcher::new(
    |data: &ExplorerDirectoryState, _| data.expanded,
    move |selector, _, _| match selector {
      true => Box::new(
        Flex::column()
          .with_child(directory_button(insets))
          .with_child(explorer(indent + 1.0)),
      ),
      false => Box::new(directory_button(insets)),
    },
  )
}

pub fn explorer(indent: f64) -> impl Widget<ExplorerDirectoryState> {
  let mut root_column: Flex<ExplorerDirectoryState> = Flex::column();

  let insets = Insets::new(
    theme::SPACING.x2 + indent * theme::SPACING.x2,
    theme::SPACING.x1 * 0.5,
    theme::SPACING.x2,
    theme::SPACING.x1 * 0.5,
  );

  let directory_list: List<ExplorerDirectoryState> =
    List::new(move || directory_entry(indent, insets));

  let file_list: List<ExplorerFileState> = List::new(move || {
    Container::new(
      Flex::row()
        .main_axis_alignment(MainAxisAlignment::Start)
        .cross_axis_alignment(CrossAxisAlignment::Center)
        .with_child(
          Svg::new(assets::FILE_TEXT_ICON.parse::<SvgData>().unwrap())
            .fix_height(default_theme::TEXT_SIZE_NORMAL),
        )
        .with_spacer(theme::SPACING.x1)
        .with_child(
          Label::new(|data: &ExplorerFileState, _env: &_| data.name.clone())
            .with_text_size(default_theme::TEXT_SIZE_NORMAL)
            .expand_width(),
        ),
    )
    .expand_width()
    .padding(insets)
    .background(Painter::new(|ctx, data: &ExplorerFileState, _| {
      if ctx.is_active() || data.is_focused {
        ctx
          .render_ctx
          .fill(ctx.size().to_rect(), &Color::rgba8(255, 255, 255, 13));
      }
    }))
    .on_click(|ctx, data, _| {
      ctx.set_active(true);
      ctx.submit_command(Command::new(
        commands::EXPLORER_CLICK_FILE,
        data.clone(),
        Target::Auto,
      ));
    })
  });

  root_column
    .add_child(directory_list.lens(ExplorerDirectoryState::directories));

  root_column.add_child(file_list.lens(ExplorerDirectoryState::files));

  root_column
}
