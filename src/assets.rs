// Icons
pub const FILE_TEXT_ICON: &str =
  include_str!("../assets/icons/feather-file-text.svg");
//pub const FILE_ICON: &str = include_str!("../assets/icons/feather-file.svg");
pub const CHEVRON_RIGHT_ICON: &str =
  include_str!("../assets/icons/feather-chevron-right.svg");
pub const CHEVRON_DOWN_ICON: &str =
  include_str!("../assets/icons/feather-chevron-down.svg");

// Themes
pub const GRUVBOX_THEME: &str =
  include_str!("../assets/themes/gruvbox.tmTheme");

pub const _TOMORROW_NIGHT_EIGHTIES_THEME: &str =
  include_str!("../assets/themes/Tomorrow-Night-Eighties.tmTheme");
