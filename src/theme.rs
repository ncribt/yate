use druid::Color;

pub struct Palette {
  pub background: Color,
  pub background_2: Color,
  pub text: Color,
  pub border: Color,
}

pub const PALETTE: Palette = Palette {
  background: Color::rgba8(41, 40, 40, 255),
  background_2: Color::rgba8(28, 28, 28, 255),
  text: Color::rgba8(212, 190, 152, 255),
  border: Color::rgba8(28, 28, 28, 255),
};

pub struct Spacing {
  pub x1: f64,
  pub x2: f64,
  pub x4: f64,
  pub x8: f64,
  pub x16: f64,
}

pub const SPACING: Spacing =
  Spacing { x1: 4.0, x2: 8.0, x4: 16.0, x8: 32.0, x16: 64.0 };

pub struct FontSizes {
  pub normal: f64,
}

pub struct Font {
  pub font_sizes: FontSizes,
}

pub const FONT: Font = Font { font_sizes: FontSizes { normal: 14.0 } };
