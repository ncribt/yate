use std::{
  fs::{read_dir, FileType},
  path::Path,
};

use druid::{im::Vector, Data, Lens};

#[derive(Debug, Clone, Data, Lens, Default)]
pub struct ExplorerFileState {
  pub name: String,
  pub path: String,
  pub is_focused: bool,
}

#[derive(Debug, Clone, Data, Lens)]
pub struct ExplorerDirectoryState {
  pub name: String,
  pub path: String,
  pub expanded: bool,
  pub initiated: bool,
  pub files: Vector<ExplorerFileState>,
  pub directories: Vector<ExplorerDirectoryState>,
}

impl Default for ExplorerDirectoryState {
  fn default() -> Self {
    Self {
      name: String::from(""),
      path: String::from(""),
      expanded: false,
      initiated: false,
      files: Vector::new(),
      directories: Vector::new(),
    }
  }
}

impl ExplorerDirectoryState {
  pub fn toggle_expanded(&mut self) {
    self.expanded = !self.expanded;

    if !self.initiated {
      self.init();
    }
  }

  pub fn init(&mut self) {
    let (directories, files) = read_directory(self.path.clone());

    self.directories = directories;
    self.files = files;
    self.initiated = true;
  }
}

#[derive(Debug, Clone, Data, Lens, Default)]
pub struct ExplorerState {
  pub directory: ExplorerDirectoryState,
  pub selected_path: Option<String>,
}

impl ExplorerState {
  pub fn new(root_path: String) -> Self {
    let (directories, files) = read_directory(root_path.clone());

    Self {
      directory: ExplorerDirectoryState {
        name: String::from("."),
        path: root_path,
        expanded: true,
        initiated: true,
        files,
        directories,
      },
      selected_path: None,
    }
  }

  pub fn select_path(&mut self, path: String) {
    self.selected_path = Some(path);
  }
}

fn read_directory(
  path: String,
) -> (Vector<ExplorerDirectoryState>, Vector<ExplorerFileState>) {
  let mut directories: Vector<ExplorerDirectoryState> = Vector::new();
  let mut files: Vector<ExplorerFileState> = Vector::new();

  let entries = read_dir(Path::new(&path)).unwrap().map(|entry| entry.unwrap());
  let mut entries = entries
    .map(|entry| {
      (
        entry.file_type().unwrap(),
        entry.file_name().to_str().unwrap().to_string(),
        entry.path().to_str().unwrap().to_string(),
      )
    })
    .collect::<Vector<(FileType, String, String)>>();

  entries.sort_by(|a, b| a.1.cmp(&b.1));

  for (file_type, name, path) in entries {
    if file_type.is_file() {
      files.push_back(ExplorerFileState { name, path, is_focused: false });
      continue;
    }

    if file_type.is_dir() {
      directories.push_back(ExplorerDirectoryState {
        name,
        path,
        expanded: false,
        initiated: false,
        directories: Vector::new(),
        files: Vector::new(),
      });
    }
  }

  (directories, files)
}
