use druid::{Data, Lens};

use super::{EditorState, ExplorerState};

#[derive(Debug, Clone, Data, Lens, Default)]
pub struct RootState {
  pub explorer_state: ExplorerState,
  pub editor_state: EditorState,
}
