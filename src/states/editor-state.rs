use druid::{im::Vector, Data, Lens};
use ropey::Rope;

#[derive(Debug, Clone, Data, Lens)]
pub struct EditorFileState {
  pub path: String,
  #[data(eq)]
  pub cached_content: Option<Rope>,
}

impl EditorFileState {
  pub fn new(path: String) -> Self {
    Self { path, cached_content: None }
  }

  pub fn cache_content(&mut self, content: Rope) {
    self.cached_content = Some(content);
  }
}

#[derive(Debug, Clone, Data, Lens)]
pub struct EditorState {
  pub focused_path: Option<String>,
  pub files: Vector<EditorFileState>,
}

impl Default for EditorState {
  fn default() -> Self {
    Self { focused_path: None, files: Vector::new() }
  }
}

impl EditorState {
  pub fn focus_file(&mut self, path: String) {
    let mut exists = false;

    for file in self.files.iter_mut() {
      if file.path == path {
        exists = true;
      }
    }

    if exists == false {
      let new_file = EditorFileState::new(path.clone());
      self.files.push_back(new_file.clone());
    }

    self.focused_path = Some(path.clone());
  }

  pub fn get_focused_file(&self) -> Option<EditorFileState> {
    if let Some(focused_path) = &self.focused_path {
      for file in self.files.iter() {
        if file.path == *focused_path {
          return Some(file.clone());
        }
      }
    }

    None
  }

  pub fn update_focused_file(&mut self, updated_file: EditorFileState) {
    if let Some(focused_path) = &self.focused_path {
      let mut updated_files: Vector<EditorFileState> = Vector::new();

      for file in self.files.iter() {
        if file.path == *focused_path {
          updated_files.push_back(updated_file.clone());
        } else {
          updated_files.push_back(file.clone());
        }
      }

      self.files = updated_files;
    }
  }

  pub fn cache_file_content(&mut self, path: String, content: Rope) {
    let mut updated_files: Vector<EditorFileState> = Vector::new();

    for file in self.files.iter_mut() {
      if file.path == path {
        file.cache_content(content.clone());
      }

      updated_files.push_back(file.clone());
    }

    self.files = updated_files;
  }
}
