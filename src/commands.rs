use druid::Selector;
use ropey::Rope;

use crate::states::{ExplorerDirectoryState, ExplorerFileState};

pub const EXPLORER_CLICK_FILE: Selector<ExplorerFileState> =
  Selector::new("explorer.click-file");

pub const EXPLORER_CLICK_DIRECTORY: Selector<ExplorerDirectoryState> =
  Selector::new("explorer.click-directory");

pub const EDITOR_SCROLL_TO: Selector<(f64, f64)> =
  Selector::new("editor.scroll-to");

pub const EDITOR_SCROLL: Selector<(f64, f64)> = Selector::new("editor.scroll");

pub const EDITOR_SET_CURSOR: Selector<()> = Selector::new("editor.set-cursor");

pub const EDITOR_CACHE_FILE_CONTENT: Selector<(String, Rope)> =
  Selector::new("editor.cache-file-content");

pub const EDITOR_LEN_LINES_CHANGED: Selector<usize> =
  Selector::new("editor.len-lines-changed");

pub const EDITOR_RESET_GUTTER: Selector<()> =
  Selector::new("editor.reset-gutter");

pub const EDITOR_RESET_SCROLL_BAR: Selector<()> =
  Selector::new("editor.reset-scroll-bar");

pub const EDITOR_SET_DRAGGING_SCROLL_BAR: Selector<bool> =
  Selector::new("editor.set-dragging-scroll-bar");
