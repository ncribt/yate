# Yate

### **Y**et **A**nother **T**ext **E**ditor

Pet project to improve my knowledge of Rust and learn about [Druid](https://github.com/linebender/druid).

## Warning

The app is not usable yet, you can currently open a directory, explore the files in the "Explorer" (left panel) and view the content of a file in the "Editor" (right panel) but you cannot modify/save the content of a file yet.

## Screenshot

![capture](/screenshots/screenshot.png?raw=true "Screenshot")
